<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Careers */
/* @var $form yii\widgets\ActiveForm */
?>
<ul class="nav nav-tabs nav-pills" id="menu_tabs" role="tablist">
    <li role="presentation" class="active"><a href="#main" aria-controls="main" role="tab" data-toggle="tab">Main</a></li>
    <li role="presentation"><a href="#metatags" aria-controls="metatags" role="tab" data-toggle="tab">Meta tags</a></li>
</ul>

<div class="careers-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="tab-content">
			<div role="tabpanel" class="tab-pane active" id="main">

				<?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

				<?= $form->field($model, 'url')->textInput(['maxlength' => true]) ?>
		
				<div class="form-group field-careers-text">
					<label class="control-label" for="careers-text">Text</label>
					<textarea id="careers-text" class="form-control" name="Careers[text]" rows="6" aria-invalid="false"><?=$model->text?></textarea>
				</div>
    
				<div class="form-group field-services-icon has-success">
					<label class="control-label" for="services-icon">Icon</label>
					<span id="selected-icon"><?php if($model->icon) echo "<i class='".$model->icon."'></i>" ?></span>
					<button type="button" id="servicesIcons" class="btn btn-primary" data-target="#myModal">
						Select Icon
					</button>
					<input type="hidden" id="services-icon" class="form-control" name="Careers[icon]">
				</div>
    
				<!-- Modal -->
				<div class="modal fade bs-example-modal-lg" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
					<div class="modal-dialog modal-lg" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								<h4 class="modal-title" id="myModalLabel">Icons</h4>

							</div>
							<div class="modal-body">						
									<?=$this->render('icons')?>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
								<button type="button" class="btn btn-primary">Save changes</button>
							</div>
						</div>
					</div>
				</div>
				
			</div>
			<div role="tabpanel" class="tab-pane" id="metatags">

				<?= $form->field($mtModel, 'meta_title')->textInput(['maxlength' => true]) ?>

				<?= $form->field($mtModel, 'meta_desctiption')->textInput(['maxlength' => true]) ?>

				<?= $form->field($mtModel, 'meta_keywords')->textInput(['maxlength' => true]) ?>

				<?= $form->field($mtModel, 'ogt_title')->textInput(['maxlength' => true]) ?>

				<?= $form->field($mtModel, 'ogt_description')->textInput(['maxlength' => true]) ?>

				<?= $form->field($mtModel, 'image')->fileInput() ?>

				<?php if($mtModel->ogt_image): ?>
					<div class="form-group field-careers-ogt_description">
					<img src="/<?=$mtModel->ogt_image?>" width="300px">
				</div>
				<?php endif; ?>
			</div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
