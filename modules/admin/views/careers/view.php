<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Careers */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Careers'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="careers-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            'url:url',
						[
							'format' => 'raw',
							'attribute' => 'icon',
							'value' => function($data){return '<span id="selected-icon">'. ($data->icon ? "<i class='".$data->icon."'></i>" : '') . '</span>';}
						],
            'text:html',
        ],
    ]) ?>
    
    <h3>Meta tags</h3>
    <?= DetailView::widget([
        'model' => $mtModel,
        'attributes' => [
            'meta_title',
            'meta_desctiption',
            'meta_keywords',
            'ogt_title',
            'ogt_description',
						[
							'format' => 'raw',
							'attribute' => 'ogt_image',
							'value' => '<image src="/'.$mtModel->ogt_image.'" width="300px">'
						],
        ],
    ]) ?>

</div>
