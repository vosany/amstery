<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Settings */
/* @var $form yii\widgets\ActiveForm */
$inputOpt = Yii::$app->controller->action->id == 'update' ? ['disabled' => 'disabled'] : [];
$inputOpt['maxlength'] = true;
$inputOpt['placeholder'] = 'Example Title';
?>

<div class="settings-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput($inputOpt) ?>
    
		<?php $inputOpt['placeholder'] = 'example_name' ?>
    <?= $form->field($model, 'name')->textInput($inputOpt) ?>

    <?= $form->field($model, 'value')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
