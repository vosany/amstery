<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Settings */

$this->title = 'Update Settings';
$this->params['breadcrumbs'][] = ['label' => 'Settings', 'url' => ['edit']];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="settings-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="settings-form">
    	<ul class="nav nav-tabs" role="tablist">  
				<?php foreach ($groups as $group): ?>					
  			  <li role="presentation"<?= $group->id == 1 ? ' class="active"' : ''?>>
  			  	<a href="#<?=$group->tabId?>" aria-controls="<?=$group->tabId?>" role="tab" data-toggle="tab"><?=ucfirst($group->name)?></a>
  			  </li>
				<?php endforeach; ?>
			</ul>
			<?php $form = ActiveForm::begin(); ?>
				
				<div class="tab-content">
					<?=$tabs?>
				</div>

				<div class="form-group">
						<?= Html::submitButton('Update', ['class' => 'btn btn-primary']) ?>
				</div>

			<?php ActiveForm::end(); ?>

	</div>

</div>
