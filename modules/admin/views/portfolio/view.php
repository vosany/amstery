<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Portfolio */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Portfolios', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="portfolio-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
						[
							'format' => 'raw',
							'attribute' => 'site',
							'value' => function ($data) {return Html::a($data->site, [$data->site]);}
						],
            'about:html',
            'wedid:html',
						[
							'format' => 'raw',
							'attribute' => 'workers',
							'value' => function ($data) {return $data->prettyworkers;}
						],
						[
							'format' => 'raw',
							'attribute' => 'started',
							'value' => function ($data) {return date('M Y', $data->started);}
						],
						[
							'format' => 'raw',
							'attribute' => 'finished',
							'value' => function ($data) {return date('M Y', $data->finished);}
						],
            'order',
        ],
    ]) ?>

</div>
