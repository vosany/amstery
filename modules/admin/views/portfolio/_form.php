<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Portfolio */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="portfolio-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'site')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'icon')->textInput(['maxlength' => true]) ?>
		
		<div class="form-group field-portfolio-about">
			<label class="control-label" for="portfolio-about">About Company</label>
			<textarea id="portfolio-about" class="form-control" name="Portfolio[about]" rows="6" aria-invalid="false"><?=$model->about?></textarea>
		</div>
		
		<div class="form-group field-portfolio-wedid">
			<label class="control-label" for="portfolio-wedid">What We Did</label>
			<textarea id="portfolio-wedid" class="form-control" name="Portfolio[wedid]" rows="6" aria-invalid="false"><?=$model->wedid?></textarea>
		</div>
		
		<div class="form-group field-portfolio-workers">
			<label class="control-label" for="portfolio-workers">People on the project</label>
			<select id="portfolio-workers" class="form-control" name="Portfolio[workers][]"  multiple="multiple">
				<option>Select User</option>
				<?=$workers?>
			</select>
		</div>
		
		<div class="form-group field-portfolio-started">
			<label class="control-label" for="portfolio-started">Started project</label>
			<input id="portfolio-started" type="text" name="Portfolio[started]" class="form-control" value="<?=$model->started?>">
		</div>
		
		<div class="form-group field-portfolio-finished">
			<label class="control-label" for="portfolio-finished">Finished project</label>
			<input id="portfolio-finished" type="text" name="Portfolio[finished]" class="form-control" value="<?=$model->finished?>">
		</div>
	

    <?= $form->field($model, 'order')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
