<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Portfolios';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="portfolio-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Portfolio', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
						[
							'format' => 'raw',
							'attribute' => 'site',
							'value' => function($data) { return Html::a('Open', [$data->site], ['target' => '_blank']); },
						],
						[
							'format' => 'raw',
							'attribute' => 'started',
							'value' => function($data) { return date('M Y', $data->started); },
						],
						[
							'format' => 'raw',
							'attribute' => 'finished',
							'value' => function($data) { return date('M Y', $data->finished); },
						],
            // 'order',
						[
							'format' => 'raw',
							'value' => function ($data) { return 
										'<a class="btn btn-info" href="/admin/portfolio/view/?id='.$data->id.'"><i class="fa fa-eye"></i></a> '.
										'<a class="btn btn-warning" href="/admin/portfolio/update/?id='.$data->id.'"><i class="fa fa-pencil"></i></a> '.
										'<a class="btn btn-danger" href="/admin/portfolio/delete/?id='.$data->id.'"><i class="fa fa-trash"></i></a>';
									}
						],
        ],
    ]); ?>
</div>
