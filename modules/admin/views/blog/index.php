<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app','Blogs');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="blog-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app','Create Blog'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
						'url',
            [
                'attribute' => 'img',
                'format'=>'html',
                'value' => function($data){
										return Html::img(Yii::getAlias('@web').'/img/blog/'. $data->img, ['width' => '70px']);
                }
            ],
            ['format' => 'raw','attribute' => 'datetime','format' => ['date', 'php:d.m.Y H:i:s'] ],
            // 'short_desc_ru:ntext',
            // 'short_desc_en:ntext',
            // 'full_desc_ru:ntext',
            // 'full_desc_en:ntext',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
