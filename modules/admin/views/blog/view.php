<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Blog */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Blogs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="blog-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app','Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app','Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'title',
						'url',
            [
                'attribute'=>'img',
                'value'=>Yii::getAlias("@web/img/blog/".$model->img),
                'format'=>'image'
            ],
            ['format' => 'raw','attribute' => 'datetime','format' => ['date', 'php:d.m.Y H:i:s'] ],
            'short_desc:ntext',
            'full_desc:ntext',
        ],
    ]) ?>
    
    <h3>Meta tags</h3>
    <?= DetailView::widget([
        'model' => $mtModel,
        'attributes' => [
            'meta_title',
            'meta_desctiption',
            'meta_keywords',
            'ogt_title',
            'ogt_description',
						[
							'format' => 'raw',
							'attribute' => 'ogt_image',
							'value' => '<image src="/'.$mtModel->ogt_image.'" width="300px">'
						],
        ],
    ]) ?>

</div>
