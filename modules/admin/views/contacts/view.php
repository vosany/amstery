<?php 

?>


<section class="content-header">
	<h1>		
		<a href="/admin/contacts" class="btn btn-default btn-sm" data-toggle="tooltip" data-container="body" title="" data-original-title="Back">
			<i class="fa fa-reply"></i>
		</a>
		Read Mail
	</h1>
</section>

<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-primary">
				<div class="box-header with-border">
					<div class="box-tools pull-right">
						<a href="#" class="btn btn-box-tool" data-toggle="tooltip" title="" data-original-title="Previous"><i class="fa fa-chevron-left"></i></a>
						<a href="#" class="btn btn-box-tool" data-toggle="tooltip" title="" data-original-title="Next"><i class="fa fa-chevron-right"></i></a>
					</div>
				</div>
				<!-- /.box-header -->
				<div class="box-body no-padding">
					<div class="mailbox-read-info">
						<h3>From <?=$model->name?></h3>
						<h5>
							Email: <?=$model->email?>
						</h5>
						<h5>
							Phone: <?=$model->phone?>
							<span class="mailbox-read-time pull-right"><?=date('d-m-Y H:i:s', $model->created)?></span>
						</h5>
					</div>
					<!-- /.mailbox-read-info -->
					<div class="mailbox-controls with-border text-center">
						<div class="btn-group">
							<button type="button" class="btn btn-default btn-sm" data-toggle="tooltip" data-container="body" title="" data-original-title="Delete">
								<i class="fa fa-trash-o"></i></button>
							<button type="button" class="btn btn-default btn-sm" data-toggle="tooltip" data-container="body" title="" data-original-title="Reply">
								<i class="fa fa-reply"></i></button>
							<button type="button" class="btn btn-default btn-sm" data-toggle="tooltip" data-container="body" title="" data-original-title="Forward">
								<i class="fa fa-share"></i></button>
						</div>
						<!-- /.btn-group -->
						<button type="button" class="btn btn-default btn-sm" data-toggle="tooltip" title="" data-original-title="Print">
							<i class="fa fa-print"></i></button>
					</div>
					<!-- /.mailbox-controls -->
					<div class="mailbox-read-message">
						<p><?=$model->message?></p>
					</div>
					<!-- /.mailbox-read-message -->
				</div>
				<div class="box-footer">
					<div class="pull-right">
						<button type="button" class="btn btn-default"><i class="fa fa-reply"></i> Reply</button>
						<button type="button" class="btn btn-default"><i class="fa fa-share"></i> Forward</button>
					</div>
					<button type="button" class="btn btn-default"><i class="fa fa-trash-o"></i> Delete</button>
					<button type="button" class="btn btn-default"><i class="fa fa-print"></i> Print</button>
				</div>
				<!-- /.box-footer -->
			</div>
			<!-- /. box -->
		</div>
		<!-- /.col -->
	</div>
	<!-- /.row -->
</section>