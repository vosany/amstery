<?php

namespace app\modules\admin\controllers;

use app\models\ContactForm;

class ContactsController extends \yii\web\Controller
{
    public function actionIndex()
    {
				$model = ContactForm::find()->orderBy('read,created')->all();
        return $this->render('index', ['model' => $model]);
    }
	
		public function actionView($id)
		{
			$model = ContactForm::findOne($id);
			$this->unreadStatus($model);
			
			if($model) {
				return $this->render('view', ['model' => $model]);
			} else {
				return $this->redirect(Yii::$app->request->referrer);
			}
		}
	
		private function unreadStatus($model)
		{
			if ($model->read == 0) {
				$model->read = 1;
				if (!$model->save()) {
					return false;
				}
			}
			return true;
		}

}
