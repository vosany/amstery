<?php

namespace app\modules\admin\controllers;

use Yii;
use app\models\Menu;
use yii\data\ActiveDataProvider;
use yii\web\Response;

class MenuController extends \yii\web\Controller
{
		public function beforeAction($action)
		{            
				if ($action->id == 'order' || $action->id == 'save') {
						$this->enableCsrfValidation = false;
				}

				return parent::beforeAction($action);
		}
    public function actionIndex()
    {
				$dataProvider = new ActiveDataProvider( [
						'query' => Menu::find( )->orderBy('order'),
						'sort' => false,
						'pagination' => false
				] );
				$this->enableCsrfValidation = false;
				return $this->render( 'index', [
						'dataProvider' => $dataProvider,
				] );
				//$model = Menu::find()->orderBy('order')->all();
        //return $this->render('index', ['model' => $model]);
    }
	
		public function actionSave()
		{
				$post = Yii::$app->request->post();
				Yii::$app->response->format = Response::FORMAT_JSON;
			
				$id = $post['id'];
				$type = $post['type'];
				$value = $post['value'];
				
				if ($model = Menu::findOne($id)) {
						if ($type == 'title') {
								$model->title = $value;
								$model->save();
								return 'success';
						}
						if ($type == 'url') {
								$model->url = $value;
								$model->save();								
								return 'success';
						}
				}
		}
	
		public function actionOrder()  
		{
				$allMenu = Menu::find()->orderBy('order')->all();
			
				foreach ($allMenu as $menu) {
						$menu->order = $menu->order * 10 + 1;
						$menu->save();
				}
        $post = Yii::$app->request->post();
				
        if (isset( $post['key'], $post['pos'] ))   {
            $model = $this->findModel($post['key']);
						$model->order = ((int)$post['pos'] + 2) * 10;
						$model->save();
        }
			
				$newPos = 1;
				$allMenu = Menu::find()->orderBy('order')->all();
			
				foreach ($allMenu as $menu) {
						$menu->order = $newPos;
						$menu->save();
						$newPos++;
				}	
    }
		
    protected function findModel($id)
    {			
        if (($model = Menu::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
