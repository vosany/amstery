$(document).ready(function(){
	var location = window.location.href.split('.com');
	if (location[1] == '/#services') {
		$('html, body').animate({
	        scrollTop: $("#services").offset().top - 70
	    }, 1000);
	}
	
	var sliderOptions = {
		slideWidth: 380,
		minSlides: 1,
		maxSlides: 3,
		moveSlides: 1,
		pager: false,
		controls: false,
		speed: 200,
		adaptiveHeight: false,
		autoStart: false,
		autoHover: true,
	};
	var sliderService = $('.bxsliderService').bxSlider(sliderOptions);
	var sliderTeam = $('.bxsliderTeam').bxSlider(sliderOptions);
	
	$('.carousel-control').on('click', function(event){
		event.preventDefault();
		
		var controleType = $(this).attr('data-slide');
		var section = $(this).attr('data-section');
		
		if (section == 'service') {
			if(controleType == 'prev') {
				sliderService.goToPrevSlide();
			}
			if(controleType == 'next') {
				sliderService.goToNextSlide();
			}	
		}
		
		if (section == 'team') {
			if(controleType == 'prev') {
				sliderTeam.goToPrevSlide();
			}
			if(controleType == 'next') {
				sliderTeam.goToNextSlide();
			}
		}		
	});
	
	$('#site-index-service').on('click', function(event) {
		event.preventDefault();
	    $('html, body').animate({
	        scrollTop: $("#services").offset().top - 70
	    }, 1000);
	});
	
	$('a[href*="#"]').bind("click", function(e){console.log(location);
		if(location[1] == '/') {
			var anchor = $(this);
			$('html, body').stop().animate({
			 scrollTop: $(anchor.attr('href')).offset().top-100
			}, 1000);
			e.preventDefault();
		} else { 
			e.preventDefault();
			window.location.href = '/' + $(this).attr('href');
		}
   });
});

