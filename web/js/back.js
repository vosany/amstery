$('.bs-glyphicons-list li').on('click', function(){
	var classData = $(this).find('.glyphicon').attr('class');
	$('#services-icon').val(classData);
	$('#selected-icon').html('<i class="'+classData+'"></i>');
	$('.modal-footer .btn-default').click();
});

$('#menu .editMenuDiv').on('click', 'input', function(event){
	var menuInput = $(this);
	if($(this)[0].readOnly) {
		checkFieldsBeforeEdit(menuInput);
	} else {
		
	}
});

function checkFieldsBeforeEdit(elem) {
		$('#menu .editMenuDiv input').each(function(i,val){
			if (val.readOnly == false) {
				$(this).next().click();
			}
			console.log(val.readOnly);
		});
		startEditMenu(elem);	
}
		
function startEditMenu(elem) {
	elem[0].readOnly = false;
	elem.next().show();
}

$('.menuLoading').on("click", function(event){
	var button = $(this);
	var input = $(this).prev();
	var menuLabel = input.val();
	var menuId = input.attr('data-id');
	var menuType = input.attr('data-type');
	
	$.post('/admin/menu/save', {'type':menuType, 'value':menuLabel, 'id':menuId}, function(data){		
		if(data == 'success') {
			input.val(menuLabel);
			input[0].readOnly = true;		
			button.hide();
			input.parent().addClass('has-success');
			input.blur();
			setTimeout(function(){input.parent().removeClass('has-success')}, 3000);
		}
	});
});

$(function () {
    //Enable iCheck plugin for checkboxes
    //iCheck for checkbox and radio inputs
    $('.mailbox-messages input[type="checkbox"]').iCheck({
      checkboxClass: 'icheckbox_flat-blue',
      radioClass: 'iradio_flat-blue'
    });

    //Enable check and uncheck all functionality
    $(".checkbox-toggle").click(function () {
      var clicks = $(this).data('clicks');
      if (clicks) {
        //Uncheck all checkboxes
        $(".mailbox-messages input[type='checkbox']").iCheck("uncheck");
        $(".fa", this).removeClass("fa-check-square-o").addClass('fa-square-o');
      } else {
        //Check all checkboxes
        $(".mailbox-messages input[type='checkbox']").iCheck("check");
        $(".fa", this).removeClass("fa-square-o").addClass('fa-check-square-o');
      }
      $(this).data("clicks", !clicks);
    });
	
		$(".mailbox-messages input[type='checkbox']").on('click', function(event){
			var clicks = $(this).parent.data('aria-checked');
			console.log(clicks);
		});

    $(".mailbox-star").click(function (e) {
      e.preventDefault();
      //detect type
      var $this = $(this).find("a > i");
      var glyph = $this.hasClass("glyphicon");
      var fa = $this.hasClass("fa");

      //Switch states
      if (glyph) {
        $this.toggleClass("glyphicon-star");
        $this.toggleClass("glyphicon-star-empty");
      }

      if (fa) {
        $this.toggleClass("fa-star");
        $this.toggleClass("fa-star-o");
      }
    });

		$('#servicesIcons').on('click', function(){
			$('#myModal').modal('toggle');
		});

		if(typeof(CKEDITOR) != "undefined" && CKEDITOR !== null) {
			if ($('#portfolio-about').length > 0) {
				CKEDITOR.replace('portfolio-about');
			}
			if ($('#portfolio-wedid').length > 0) {
				CKEDITOR.replace('portfolio-wedid');
			}
			if ($('#blog-short_desc').length > 0) {
				CKEDITOR.replace('blog-short_desc');
			}
			if ($('#blog-full_desc').length > 0) {
				CKEDITOR.replace('blog-full_desc');
			}
			if ($('#pages-text').length > 0) {
				CKEDITOR.replace('pages-text');
			}
			if ($('#team-about').length > 0) {
				CKEDITOR.replace('team-about');
			}
			if ($('#careers-text').length > 0) {
				CKEDITOR.replace('careers-text');
			}
		}
});
var datePickerOption = {
    format: "MM yyyy",
    startView: 1,
    minViewMode: 1,
    maxViewMode: 2,
    autoclose: true
};

$('#portfolio-started').datepicker(datePickerOption);
$('#portfolio-finished').datepicker(datePickerOption);
$('#portfolio-workers').select2();