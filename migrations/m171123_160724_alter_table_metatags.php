<?php

use yii\db\Migration;

/**
 * Class m171123_160724_alter_table_metatags
 */
class m171123_160724_alter_table_metatags extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
				$this->addColumn('metatags', 'url', $this->string());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('metatags', 'url');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171123_160724_alter_table_metatags cannot be reverted.\n";

        return false;
    }
    */
}
