<?php

use yii\db\Migration;

/**
 * Handles the creation of table `careers`.
 */
class m180105_112142_create_careers_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('careers', [
            'id' => $this->primaryKey(),
            'title' => $this->string(),
            'url' => $this->string(),
            'text' => $this->text(),
						'icon' => $this->string(),
						'mt_id' => $this->integer()->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('careers');
    }
}
