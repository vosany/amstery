<?php

use yii\db\Migration;

/**
 * Handles adding layout to table `pages`.
 */
class m180515_152410_add_layout_column_to_pages_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('pages', 'layout', $this->integer()->defaultValue(1));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('pages', 'layout');
    }
}
