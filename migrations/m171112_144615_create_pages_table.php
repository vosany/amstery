<?php

use yii\db\Migration;

/**
 * Handles the creation of table `pages`.
 */
class m171112_144615_create_pages_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('pages', [
            'id' => $this->primaryKey(),
            'title' => $this->string(),
            'url' => $this->string(),
            'text' => $this->text(),
						'mt_id' => $this->integer()->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('pages');
    }
}
