<?php

use yii\db\Migration;

/**
 * Handles the creation of table `services`.
 */
class m171123_131634_create_services_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('services', [
            'id' => $this->primaryKey(),
						'title' => $this->string(),
						'icon' => $this->string(),
						'description' => $this->string(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('services');
    }
}
