<?php

use yii\db\Migration;

/**
 * Class m171123_155808_create_insert
 */
class m171123_155808_create_insert extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
				$this->insert('settings',[
					'title' => 'Tell me more button',
					'name' => 'tell_me_more_button',
					'value' => 'Tell me More',
				]);
				$this->insert('settings',[
					'title' => 'Service text on main page',
					'name' => 'main_service_text',
					'value' => 'High quality and great perfomance.',
				]);
				$this->insert('settings',[
					'title' => 'Main page Team title',
					'name' => 'main_team_title',
					'value' => 'OUR AMAZING TEAM',
				]);
				$this->insert('settings',[
					'title' => 'Main page Team description',
					'name' => 'main_team_description',
					'value' => 'We have assembled the best team',
				]);
				$this->insert('settings',[
					'title' => 'Main page Team bottom text',
					'name' => 'main_team_bottom_text',
					'value' => 'We do everything\'s better.',
				]);
				$this->insert('services',[
					'title' => 'E-Commerce',
					'icon' => 'glyphicon glyphicon-shopping-cart',
					'description' => 'Build different sizes and trands.',
				]);
				$this->insert('services',[
					'title' => 'Responsive Design',
					'icon' => 'glyphicon glyphicon-resize-full',
					'description' => 'Users will can view your website on all their devices.',
				]);
				$this->insert('services',[
					'title' => 'Web Security',
					'icon' => 'glyphicon glyphicon-lock',
					'description' => 'We make only safe solution for your business.',
				]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
				$this->truncateTable('settings');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171123_155808_create_insert cannot be reverted.\n";

        return false;
    }
    */
}
