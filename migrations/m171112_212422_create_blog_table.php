<?php

use yii\db\Migration;

/**
 * Handles the creation of table `blog`.
 */
class m171112_212422_create_blog_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('blog', [
            'id' => $this->primaryKey(),
            'title' => $this->string(255)->notNull(),
            'url' => $this->string(255)->notNull(),
            'img' => $this->string(255),
						'mt_id' => $this->integer()->notNull(),
            'datetime' => $this->string(255)->notNull(),
            'short_desc' => $this->text()->notNull(),
            'full_desc' => $this->text()->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('blog');
    }
}
