<?php

use yii\db\Migration;

/**
 * Handles the creation of table `contact`.
 */
class m171208_155506_create_contact_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('contact', [
            'id' => $this->primaryKey(),
			'name' => $this->string(),
			'email' => $this->string(), 
			'phone' => $this->string(), 
			'subject' => $this->string(), 
			'message' => $this->string(),
			'created' => $this->integer(),
			'read' => $this->integer(1)->defaultValue(0),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('contact');
    }
}
