<?php

use yii\db\Migration;

/**
 * Handles the creation of table `portfolio`.
 */
class m171207_112130_create_portfolio_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('portfolio', [
            'id' => $this->primaryKey(),
						'name' => $this->string()->notNull(),
						'site' => $this->string()->notNull(),
						'icon' => $this->string(),
						'about' => $this->text(),
						'wedid' => $this->text(),
						'workers' => $this->string(),
						'started' => $this->integer(),
						'finished' => $this->integer(),
						'order' => $this->integer()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('portfolio');
    }
}
