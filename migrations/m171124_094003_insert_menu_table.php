<?php

use yii\db\Migration;

/**
 * Class m171124_094003_insert_menu_table
 */
class m171124_094003_insert_menu_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
				$this->insert('menu',[
					'title' => 'Services',
					'url' => '/#services',
					'order' => 1,
				]);
				$this->insert('menu',[
					'title' => 'Portfolio',
					'url' => '/portfolio',
					'order' => 2,
				]);
				$this->insert('menu',[
					'title' => 'About',
					'url' => '/about',
					'order' => 3,
				]);
				$this->insert('menu',[
					'title' => 'Blog',
					'url' => '/blog',
					'order' => 4,
				]);
				$this->insert('menu',[
					'title' => 'Team',
					'url' => '/team',
					'order' => 5,
				]);
				$this->insert('menu',[
					'title' => 'Contact',
					'url' => '#contact',
					'order' => 6,
				]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->truncateTable('settings');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171124_094003_insert_menu_table cannot be reverted.\n";

        return false;
    }
    */
}
