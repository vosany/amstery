<?php

use yii\db\Migration;

/**
 * Handles the creation of table `menu`.
 */
class m171124_093257_create_menu_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('menu', [
            'id' => $this->primaryKey(),
						'title' => $this->string()->notNull(),
						'url' => $this->string()->notNull(),
						'order' => $this->integer()->notNull()->defaultValue(1),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('menu');
    }
}
