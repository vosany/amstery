<?php

use yii\db\Migration;

/**
 * Handles the creation of table `metatags`.
 */
class m171113_205435_create_metatags_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('metatags', [
            'id' => $this->primaryKey(),
            'meta_title' => $this->string(),
            'meta_desctiption' => $this->string(),
            'meta_keywords' => $this->string(),
            'ogt_title' => $this->string(),
            'ogt_description' => $this->string(),
            'ogt_image' => $this->string(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('metatags');
    }
}
