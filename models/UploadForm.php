<?php
namespace app\models;

use Yii;
use yii\web\UploadedFile;
use yii\db\ActiveRecord;
use yii\base\Model;

class UploadForm extends Model
{
    /**
     * @var UploadedFile
     */
		public $folder;
    public $src;
		
		public function __construct($folder = 'img')
		{
				$this->folder = $folder;
		}
		

    public function rules()
    {
        return [
            [['src'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg'],
        ];
    }
    
    public function upload() 
    {
        if ($this->validate()) {
          $this->src->saveAs($this->folder . '/' . $this->src->baseName . '.' . $this->src->extension);
					//$this->src = 'uploads/' . $this->src->baseName . '.' . $this->src->extension;
            return true;
        } else {
            return false;
        }
    }
} 