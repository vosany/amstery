<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "portfolio".
 *
 * @property integer $id
 * @property string $name
 * @property string $site
 * @property string $about
 * @property string $wedid
 * @property string $workers
 * @property integer $started
 * @property integer $finished
 * @property integer $order
 */
class Portfolio extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'portfolio';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'site'], 'required'],
            [['icon', 'about', 'wedid'], 'string'],
            [['started', 'finished', 'order'], 'integer'],
            [['name', 'site', 'workers'], 'string', 'max' => 255],
        ];
    }
	
		public function getPrettyworkers()
		{
			if ($this->workers) {
				$workers = Team::find()->select('name')->where(['IN', 'id', explode(',', $this->workers)])->asArray()->column();
				$this->workers = implode(', ', $workers);
			}
			return $this->workers;
		}
	
		public function getPeriod()
		{
			$period = '';
			
			if ($this->started) {
				$period .= date('M Y', $this->started);
			}
			
			if ($this->finished) {
				if ($period) {
					$period .= ' - ';
				}
				$period .= date('M Y', $this->finished);
			}
			return $period;
		}
	
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'site' => 'Site',
            'icon' => 'Icon',
            'about' => 'About',
            'wedid' => 'Wedid',
            'workers' => 'Workers',
            'started' => 'Started',
            'finished' => 'Finished',
            'order' => 'Order',
        ];
    }
}
