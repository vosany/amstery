<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "team".
 *
 * @property integer $id
 * @property string $name
 * @property string $job
 * @property string $skills
 * @property string $review
 * @property string $about
 */
class Team extends \yii\db\ActiveRecord
{
  	public $fileImage;  
	
    public static function tableName()
    {
        return 'team';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['about'], 'string'],
            [['name', 'job', 'skills', 'review', 'image', 'url'], 'string', 'max' => 255],
						[['mt_id'], 'integer'],
						[['fileImage'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg'],
        ];
    }
	
		public function upload()
    {
			if ($this->validate()) { 
					$filename = 'img/team/' . $this->fileImage->baseName . '.' . $this->fileImage->extension;
					$this->fileImage->saveAs($filename);
					$this->fileImage = null;
					return $filename;
			} else {
					return false;
			}
    }
	
		public function formatSkills($tag = 'li')
		{
			$formatedSkills = '';
			
			if (!$this->skills)
					return $formatedSkills;
				
			foreach (explode(',',$this->skills) as $skill) {
				$formatedSkills .= '<' . $tag . '>' . $skill . '</' . $tag . '>';
			}
			
			return $formatedSkills;
		}

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'job' => Yii::t('app', 'Job'),
            'skills' => Yii::t('app', 'Skills'),
            'review' => Yii::t('app', 'Review'),
            'about' => Yii::t('app', 'About'),
            'url' => Yii::t('app', 'Url'),
            'image' => Yii::t('app', 'Image'),
        ];
    }
}
