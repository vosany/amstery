<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pages".
 *
 * @property integer $id
 * @property string $title
 * @property string $url
 * @property string $text
 * @property string $meta_title
 * @property string $meta_desctiption
 * @property string $meta_keywords
 * @property string $ogt_title
 * @property string $ogt_description
 * @property string $ogt_image
 */
class Pages extends \yii\db\ActiveRecord
{
  	public $image;  
	
    public static function tableName()
    {
        return 'pages';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['text'], 'string'],
			[['mt_id', 'layout'], 'integer'],
            [['title', 'url'], 'string', 'max' => 255],
						[['image'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg'],
        ];
    }
	
		public function upload()
    {
        if ($this->validate()) { 
						$filename = 'img/pages/' . $this->image->baseName . '.' . $this->image->extension;
            $this->image->saveAs($filename);
						$this->image = null;
            return $filename;
        } else {
            return false;
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'url' => Yii::t('app', 'Url'),
            'text' => Yii::t('app', 'Text'),
            'image' => Yii::t('app', 'Ogt Image'),
        ];
    }
}
