<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "careers".
 *
 * @property integer $id
 * @property string $title
 * @property string $url
 * @property string $text
 * @property integer $mt_id
 */
class Careers extends \yii\db\ActiveRecord
{
  	public $image;  
	
    public static function tableName()
    {
        return 'careers';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['text'], 'string'],
						[['mt_id'], 'integer'],
            [['title', 'url', 'icon'], 'string', 'max' => 255],
						[['image'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'url' => 'Url',
            'text' => 'Text',
            'mt_id' => 'Mt ID',
						'icon' => 'Icon',
        ];
    }
}
