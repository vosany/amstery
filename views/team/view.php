<?php
/* @var $this yii\web\View */
?>
<section id="team" class="bg-light-gray teammate-detail">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2 class="section-heading"><?=$model->name?></h2>
                <h3 class="text-muted team-job"><?=$model->job?></h3>
            </div>
            <div id="team-detail" class="col-lg-12">
								<div class="col-md-2">
									<img src="/<?=$model->image?>" class="img-responsive img-circle" alt="">
									<div id="skills-list">
										<h5>Main Skills</h5>							
										<ul class="list-unstyled">
											<?=$model->formatSkills()?>
										</ul>
									</div>
								</div>
								<div class="col-md-10 team-about">
										<?=$model->about?>
								</div>            	
            </div>
        </div>
    </div>
</section>
