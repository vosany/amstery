<?php
/* @var $this yii\web\View */
?>
<section id="team" class="bg-light-gray">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2 class="section-heading">Our Amazing Team</h2>
                <h3 class="section-subheading text-muted">We have assembled the best team.</h3>
                <?php foreach($model as $team): ?>
									<div class="col-sm-4">
										<div class="team-member">
											<a href="/team/<?=$team->url?>"><img src="/<?=$team->image?>" class="img-responsive img-circle" alt=""></a>
                        <a href="/team/<?=$team->url?>"><h4><?=$team->name?></h4></a>
                        <p class="text-muted"><?=$team->job?></p>
                        <p><?=$team->review?></p>
                    </div>
									</div>
           			<?php endforeach; ?>
            </div>
        </div>
    </div>
</section>
