<?php

/* @var $this yii\web\View */

use yii\bootstrap\Carousel;
?>
    <!-- Header -->
    <header>
        <div class="container">
        	<a href="#services" id="site-index-service">
            	<div class="intro-text"></div>
            </a>
        </div>
    </header>

    <!-- Services Section -->
    <section id="services">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading"><?=Yii::t('app','Services')?></h2>
                    <h3 class="section-subheading text-muted"><?=Yii::$app->settings->get('main_service_text')?></h3>
                </div>
            </div>
            <div class="row text-center">
								<div class="col-md-12">
										<div class="bxsliderService">
											<?=$serviceCarousel?>
										</div>
										<a class="left carousel-control" href="#" data-section="service" data-slide="prev"><i class="glyphicon glyphicon-chevron-left"></i></a>
										<a class="right carousel-control" href="#" data-section="service" data-slide="next"><i class="glyphicon glyphicon-chevron-right"></i></a>
								</div>
            </div>
        </div>
    </section>



    <!-- Team Section -->
    <section id="team" class="bg-light-gray">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading"><?=Yii::$app->settings->get('main_team_title')?></h2>
                    <h3 class="section-subheading text-muted"><?=Yii::$app->settings->get('main_team_description')?></h3>
                </div>
            </div>
            <div class="row">
								<div class="col-md-12">
										<div class="bxsliderTeam">
											<?=$teamCarousel?>
										</div>
										<a class="left carousel-control" href="#" data-section="team" data-slide="prev"><i class="glyphicon glyphicon-chevron-left"></i></a>
										<a class="right carousel-control" href="#" data-section="team" data-slide="next"><i class="glyphicon glyphicon-chevron-right"></i></a>
								</div>
            </div>
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 text-center">
                    <p class="large text-muted"><?=Yii::$app->settings->get('main_team_bottom_text')?></p>
                </div>
            </div>
        </div>
    </section>

    <!-- Contact Section -->
    <section id="contact">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading"><?=Yii::t('app','Contact Us')?></h2>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <form name="sentMessage" id="contactForm" novalidate>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Your Name *" id="name" required data-validation-required-message="Please enter your name.">
                                    <p class="help-block text-danger"></p>
                                </div>
                                <div class="form-group">
                                    <input type="email" class="form-control" placeholder="Your Email *" id="email" required data-validation-required-message="Please enter your email address.">
                                    <p class="help-block text-danger"></p>
                                </div>
                                <div class="form-group">
                                    <input type="tel" class="form-control" placeholder="Your Phone *" id="phone" required data-validation-required-message="Please enter your phone number.">
                                    <p class="help-block text-danger"></p>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <textarea class="form-control" placeholder="Your Message *" id="message" required data-validation-required-message="Please enter a message."></textarea>
                                    <p class="help-block text-danger"></p>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-lg-12 text-center">
                                <div id="success"></div>
                                <button type="submit" class="btn btn-xl"><?=Yii::t('app','Send Message')?></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
