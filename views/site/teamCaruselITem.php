<div>
	<div class="team-member">
		<img src="<?=$image?>" class="img-responsive img-circle" alt="">
		<h4><?=$name?></h4>
		<p class="text-muted"><?=$job?></p>
	</div>
</div>