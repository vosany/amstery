<?php
use \yii\widgets\LinkPager
?>
<section id="blog">
	<div class="container">
		<div class="row">
			<?php foreach($model as $blog): ?>
					<div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
						<a class="text-center" href="/blog/<?=$blog->url?>">
							<h5><?=$blog->title?></h5>
							<img src="/img/blog/<?=$blog->img?>" alt="<?=$blog->title?>" width="100%">
						</a>
						<p>
							<h6>
								<?=mb_substr($blog->short_desc,0,100)?>...
							</h6>
						</p>
				</div>
			<?php endforeach;?>
		</div>
		<?php		echo LinkPager::widget([
				'pagination' => $pages,
		]); ?>
	</div>
</section>
