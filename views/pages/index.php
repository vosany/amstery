<?php
/* @var $this yii\web\View */
?>
<section id="<?=$page->url?>" class="bg-light-gray">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="section-heading text-center"><?=$page->title?></h1>
                <?=$page->text?>
            </div>
        </div>
    </div>
</section>
