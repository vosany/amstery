<?php

namespace app\controllers;

use app\models\Portfolio;

class PortfolioController extends BaseController
{
    public function actionIndex()
    {
				$model = Portfolio::find()->all();
        return $this->render('index', ['model' => $model]);
    }

}
