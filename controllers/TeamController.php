<?php

namespace app\controllers;

use app\models\Team;

class TeamController extends BaseController
{
    public function actionIndex()
    {
				$model = Team::find()->all();
        return $this->render('index', ['model' => $model]);
    }
	
		public function actionView($url)
		{
				$model = Team::find()->where(['url' => $url])->one();
			
				if ($model) {
        	return $this->render('view', ['model' => $model]);
				} else {
					return $this->redirect('/team');
				}
		}

}
