<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Services;
use app\models\Team;
use app\models\Settings;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['get'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
				$serviceCarousel = $this->getServices();
				$teamCarousel = $this->getTeamCarusel();
        return $this->render('index', [
							'serviceCarousel' => $serviceCarousel, 
							'teamCarousel' => $teamCarousel
				]);
    }
	
		private function getServices()
		{
				$services = Services::find()->all();
				$carousel = '';
				foreach ($services as $service) {					
					$carousel .= $this->renderpartial('serviceCaruselItem', [
						'icon' => $service->icon,
						'title' => $service->title,
						'description' => $service->description,
					]);
				}
			
				return $carousel;
		}
	
		private function getTeamCarusel()
		{
				$teammate = Team::find()->all();
				$carousel = '';
				$countElem = $items = 1;
				foreach ($teammate as $team) {					
					$carousel .= $this->renderpartial('teamCaruselITem', [
						'image' => $team->image,
						'name' => $team->name,
						'job' => $team->job,
					]);
				}
			
				return $carousel;			
		}

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return string
     */
    public function actionContact($json = false)
    {
        $model = new ContactForm();
				$contacts = Settings::find()->where(['group' => 2])->all();
			
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('contactFormSubmitted');
						
						if($json) {
							return 'success';
						} else {
            	return $this->refresh();
						}
        }
				if($json) {
					return 'error';
				} else {
					return $this->render('contact', [
							'model' => $model,
							'contacts' => $contacts,
					]);
				}
    }
    public function actionPortfolio()
    {
        return $this->render('portfolio');
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
}
