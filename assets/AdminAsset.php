<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use Yii;
use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AdminAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $sourcePath = '@bower/';
    public $css = [
			'admin-lte/css/AdminLTE.css',
			'/css/back.css',
		];
    public $js = [
			//'/adminlte/plugins/jQuery/jquery-2.2.3.min.js',
    	'https://code.jquery.com/ui/1.12.1/jquery-ui.min.js',
			'/adminlte/bootstrap/js/bootstrap.min.js',
			//'https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js',
			//'/adminlte/plugins/morris/morris.min.js',
			//'/adminlte/plugins/sparkline/jquery.sparkline.min.js',
			//'/adminlte/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js',
			//'/adminlte/plugins/jvectormap/jquery-jvectormap-world-mill-en.js',
			//'/adminlte/plugins/knob/jquery.knob.js',
			'https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js',
			//'/adminlte/plugins/daterangepicker/daterangepicker.js',
			'/adminlte/plugins/datepicker/bootstrap-datepicker.js',
			//'/adminlte/plugins/slimScroll/jquery.slimscroll.min.js',
			//'/adminlte/plugins/fastclick/fastclick.js',
			'/adminlte/plugins/select2/select2.full.js',
			'/adminlte/plugins/iCheck/icheck.min.js',
			'/adminlte/dist/js/app.min.js',
			//'/adminlte/dist/js/pages/dashboard.js',
			//'/adminlte/dist/js/demo.js',
			//'/ckeditor/ckeditor.js',
			//'/js/bootstrap-datepicker.min.js',
			//'/datetimepicker/src/js/bootstrap-datetimepicker.js',
			//'/js/admin.js',
			//'/js/back.js',
    ];
    
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];
    
    public function init()
    {
		parent::init();
		if (stripos(Yii::$app->request->url,'/create') !== FALSE) {
			$this->js[] = '/ckeditor/ckeditor.js';
		}
    	
		if (stripos(Yii::$app->request->url,'/update') !== FALSE) {
			$this->js[] = '/ckeditor/ckeditor.js';
		}
		$this->js[] = '/js/admin.js';
		$this->js[] = '/js/back.js';
    }
}
