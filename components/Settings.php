<?php

namespace app\components;
use app\models\Settings as Setting;

class Settings
{
	public function get($name)
	{
		$setting = Setting::find()->where(['name' => $name])->one();
		
		return $setting ? $setting->value : '';
	}
}