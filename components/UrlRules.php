<?php

namespace app\components;

use yii\web\UrlRuleInterface;
use yii\base\BaseObject;
use codemix\localeurls\UrlManager;
use app\models\Pages;

class UrlRules extends UrlManager
{
    public function parseRequest($request)
    {
        $pathInfo = $request->getPathInfo();
        
		if(isset($pathInfo)) {
			if($page = Pages::find()->where(['url' => $pathInfo])->one()) { 
				return [
					'pages/index', [
						'action' => $pathInfo
					]
				];
			}
		}
        return parent::parseRequest($request); // this rule does not apply
    }
}